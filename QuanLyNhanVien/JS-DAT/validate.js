function kiemTraTrung(id, idErr, dsnv) {
  let index = timKiemViTri(id, dsnv);
  if (index == -1) {
    hideMessage(idErr);
    return true;
  } else {
    showMessageErr(idErr, "Tài khoản đã tồn tại");
    return false;
  }
}

function kiemTraRong(userInput, idErr, message) {
  if (userInput.length == 0) {
    showMessageErr(idErr, message);

    return false;
  } else {
    hideMessage(idErr);
    return true;
  }
}

function kiemTraSo(value, idErr, message) {
  var reg = /^\d{4,6}$/;

  let isNumber = reg.test(value);
  if (isNumber) {
    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}

function kiemTraChu(value, idErr, message) {
  var reg =
    /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u;

  let isWords = reg.test(value);
  if (isWords) {
    hideMessage(idErr);

    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}

function kiemTraEmail(value, idErr) {
  var reg =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  let isEmail = reg.test(value);
  if (isEmail) {
    hideMessage(idErr);

    return true;
  } else {
    showMessageErr(idErr, "Email không hợp lệ");
    return false;
  }
}

function kiemTraMatKhau(value, idErr, message) {
  var reg =
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/;

  let isPassword = reg.test(value);
  if (isPassword) {
    hideMessage(idErr);

    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}
function kiemTraNgayThangNam(value, idErr, message) {
  var reg = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
  let isDate = reg.test(value);
  if (isDate) {
    hideMessage(idErr);

    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}

function kiemTraLuongCB(value, idErr, message) {
  if (value >= 1000000 && value <= 20000000) {
    hideMessage(idErr);

    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}
function kiemTraChucVu(value, idErr, message) {
  if (value == "Sếp" || value == "Trưởng phòng" || value == "Nhân viên") {
    hideMessage(idErr);
    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}

function kiemTraGioLam(value, idErr, message) {
  if (value >= 80 && value <= 200) {
    hideMessage(idErr);
    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}
