// Lấy thông tin từ form
function layThongTinTuForm() {
  var taiKhoan = document.getElementById("tknv").value;
  var hoVaTen = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var matKhau = document.getElementById("password").value;
  var ngayLam = document.getElementById("datepicker").value;
  var luongCB = document.getElementById("luongCB").value * 1;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("giolam").value * 1;
  // Tạo nhân viên
  var nv = new nhanVien(
    taiKhoan,
    hoVaTen,
    email,
    matKhau,
    ngayLam,
    luongCB,
    chucVu,
    gioLam
  );
  return nv;
}

// Render danh sách sinh viên
function renderDsnv(nvArray, viTriRender) {
  var contentHTML = "";
  for (var index = 0; index < nvArray.length; index++) {
    var currentNv = nvArray[index];
    var contentTr = `
    <tr>
    <td>${currentNv.taiKhoan}</td>
    <td>${currentNv.hoVaTen}</td>
    <td>${currentNv.email}</td>
    <td>${currentNv.ngayLam}</td>
    <td>${currentNv.chucVu}</td>
    <td>${currentNv.tongLuong()}</td>
    <td>${currentNv.xepLoai()}</td>
    <td>
    <button data-target="#myModal" data-toggle="modal" onclick="suaNv('${
      currentNv.taiKhoan
    }')"><i class="fa fa-edit"></i></button>
    <button onclick="xoaNv('${
      currentNv.taiKhoan
    }')"><i class="fa fa-trash"></i></button>
    </td>
    </tr>
    `;
    contentHTML += contentTr;
    document.getElementById(viTriRender).innerHTML = contentHTML;
  }
}

// Tìm kiếm vị trí
function timKiemViTri(id, nvArray) {
  for (var index = 0; index < nvArray.length; index++) {
    var item = nvArray[index];
    if (item.taiKhoan == id) {
      return index;
    }
  }
  return -1;
}

// Show thông tin lên form

function showThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.taiKhoan;
  document.getElementById("name").value = nv.hoVaTen;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngayLam;
  document.getElementById("luongCB").value = nv.luongCB;
  document.getElementById("chucvu").options[
    document.getElementById("chucvu").selectedIndex
  ].text = nv.chucVu;
  document.getElementById("giolam").value = nv.gioLam;
}

// Reset form
function resetForm() {
  document.getElementById("form-nv").reset();
}
// Show messages
function showMessageErr(idErr, message) {
  document.getElementById(idErr).style.display = "block";
  document.getElementById(idErr).innerHTML = message;
}
// Hide mesage
function hideMessage(idErr) {
  document.getElementById(idErr).style.display = "none";
}
