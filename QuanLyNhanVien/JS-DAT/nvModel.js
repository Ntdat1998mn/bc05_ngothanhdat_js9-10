function nhanVien(
  taiKhoan,
  hoVaTen,
  email,
  matKhau,
  ngayLam,
  luongCB,
  chucVu,
  gioLam,
  tongLuong,
  xepLoai
) {
  this.taiKhoan = taiKhoan;
  this.hoVaTen = hoVaTen;
  this.email = email;
  this.matKhau = matKhau;
  this.ngayLam = ngayLam;
  this.luongCB = luongCB;
  this.chucVu = chucVu;
  this.gioLam = gioLam;
  this.tongLuong = function () {
    if (chucVu == "Sếp") {
      tongLuong = luongCB * 3;
    } else if (chucVu == "Trưởng phòng") {
      tongLuong = luongCB * 2;
    } else {
      tongLuong = luongCB * 1;
    }
    return tongLuong.toLocaleString("VND");
  };
  this.xepLoai = function () {
    if (gioLam >= 192) {
      xepLoai = "Nhân viên xuất sắc";
    } else if (gioLam >= 176) {
      xepLoai = "Nhân viên giỏi";
    } else if (gioLam >= 160) {
      xepLoai = "Nhân viên khá";
    } else {
      xepLoai = "Nhân viên trung bình";
    }
    return xepLoai;
  };
}
