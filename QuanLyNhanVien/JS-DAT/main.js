var DSNV = "DSNV";
var dsnv = [];
function luuLocalStorage() {
  let jsonDsnv = JSON.stringify(dsnv);
  localStorage.setItem(DSNV, jsonDsnv);
}
// Khai báo biến thông báo
// Lấy dữ liệu từ localStorage
var dataJson = localStorage.getItem(DSNV);
if (dataJson !== null) {
  var nvArray = JSON.parse(dataJson);
  for (var index = 0; index < nvArray.length; index++) {
    var item = nvArray[index];
    var nv = new nhanVien(
      item.taiKhoan,
      item.hoVaTen,
      item.email,
      item.matKhau,
      item.ngayLam,
      item.luongCB,
      item.chucVu,
      item.gioLam,
      item.xepLoai,
      item.tongLuong
    );
    dsnv.push(nv);
  }
  renderDsnv(dsnv, "tableDanhSach");
}

var themNhanVien = (document.getElementById("btnThemNV").onclick = function () {
  var nv = layThongTinTuForm();
  // Varidate tài khoản
  var isVarid = true;
  var isVarid =
    kiemTraRong(nv.taiKhoan, "tbTKNV", "Tài khoản không được để rỗng") &&
    kiemTraTrung(nv.taiKhoan, "tbTKNV", dsnv) &&
    kiemTraSo(nv.taiKhoan, "tbTKNV", "Tài khoản phải là từ 4 đến 6 ký số");

  // Validate họ tên tài khoản
  isVarid =
    isVarid &&
    kiemTraRong(nv.hoVaTen, "tbTen", "Họ và tên không được để rỗng") &&
    kiemTraChu(nv.hoVaTen, "tbTen", "Họ và tên phải là chữ");
  // Validate Email
  isVarid = isVarid && kiemTraEmail(nv.email, "tbEmail");
  // Validate Password
  isVarid =
    isVarid &&
    kiemTraRong(nv.matKhau, "tbMatKhau", "Tài khoản không được để rỗng") &&
    kiemTraMatKhau(
      nv.matKhau,
      "tbMatKhau",
      "Mật khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
    );

  // Validate ngày tháng năm
  isVarid =
    isVarid &&
    kiemTraRong(nv.ngayLam, "tbNgay", "Ngày làm không được để rỗng") &&
    kiemTraNgayThangNam(nv.ngayLam, "tbNgay", "Ngày tháng năm không hợp lệ");

  // Validate lương cơ bản
  isVarid =
    isVarid &&
    kiemTraRong(nv.luongCB, "tbLuongCB", "Lương cơ bản không được để rỗng") &&
    kiemTraLuongCB(
      nv.luongCB,
      "tbLuongCB",
      "Lương cơ bản từ 1 000 000đ đến 20 000 000đ"
    );

  // Validate chức vụ
  isVarid =
    isVarid && kiemTraChucVu(nv.chucVu, "tbChucVu", "Chọn chức vụ phù hợp");

  // Validate số giờ làm
  isVarid =
    isVarid &&
    kiemTraRong(nv.gioLam, "tbGiolam", "Số giờ làm không được để rỗng") &&
    kiemTraGioLam(
      nv.gioLam,
      "tbGiolam",
      "Số giờ làm nhân viên từ 80 giờ đến 200 giờ"
    );

  if (isVarid) {
    // Thêm nhân viên vào danh sách
    dsnv.push(nv);
    // Lưu LocalStorage
    luuLocalStorage();
    // Render dan sách nhân viên
    renderDsnv(dsnv, "tableDanhSach");
    resetForm();
  }
});

// Xoá nhân viên
function xoaNv(taiKhoan) {
  var viTri = timKiemViTri(taiKhoan, dsnv);
  if (viTri !== -1) {
    dsnv.splice(viTri, 1);
    renderDsnv(dsnv, "tableDanhSach");
  }
  luuLocalStorage();
}

// Sửa nhân viên
function suaNv(id) {
  var viTri = timKiemViTri(id, dsnv);
  if (viTri == -1) return;
  var data = dsnv[viTri];
  showThongTinLenForm(data);
  // Ngăn cản user thay đổi tài khoản
  document.getElementById("tknv").disabled = true;
}

var capNhatNhanVien = (document.getElementById("btnCapNhat").onclick =
  function () {
    var data = layThongTinTuForm();
    var isVarid = true;
    // Validate họ tên tài khoản
    var isVarid =
      kiemTraRong(data.hoVaTen, "tbTen", "Họ và tên không được để rỗng") &&
      kiemTraChu(data.hoVaTen, "tbTen", "Họ và tên phải là chữ");
    // Validate Email
    isVarid = isVarid && kiemTraEmail(data.email, "tbEmail");
    // Validate Password
    isVarid =
      isVarid &&
      kiemTraRong(data.matKhau, "tbMatKhau", "Tài khoản không được để rỗng") &&
      kiemTraMatKhau(
        data.matKhau,
        "tbMatKhau",
        "Mật khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
      );

    // Validate ngày tháng năm
    isVarid =
      isVarid &&
      kiemTraRong(data.ngayLam, "tbNgay", "Ngày làm không được để rỗng") &&
      kiemTraNgayThangNam(
        data.ngayLam,
        "tbNgay",
        "Ngày tháng năm không hợp lệ"
      );

    // Validate lương cơ bản
    isVarid =
      isVarid &&
      kiemTraRong(
        data.luongCB,
        "tbLuongCB",
        "Lương cơ bản không được để rỗng"
      ) &&
      kiemTraLuongCB(
        data.luongCB,
        "tbLuongCB",
        "Lương cơ bản từ 1 000 000đ đến 20 000 000đ"
      );

    // Validate chức vụ
    isVarid =
      isVarid && kiemTraChucVu(data.chucVu, "tbChucVu", "Chọn chức vụ phù hợp");

    // Validate số giờ làm
    isVarid =
      isVarid &&
      kiemTraRong(data.gioLam, "tbGiolam", "Số giờ làm không được để rỗng") &&
      kiemTraGioLam(
        data.gioLam,
        "tbGiolam",
        "Số giờ làm nhân viên từ 80 giờ đến 200 giờ"
      );

    if (isVarid) {
      var viTri = timKiemViTri(data.taiKhoan, dsnv);
      if (viTri == -1) return;
      dsnv[viTri] = data;
      renderDsnv(dsnv, "tableDanhSach");
      luuLocalStorage();
      // Bỏ phần ngăn user thay đổi tài khoản
      document.getElementById("tknv").disabled = false;
      resetForm();
    }
  });
// Tìm kiếm
var layXepLoai = function (trTag) {
  return trTag.xepLoai();
};
var search = (document.getElementById("btnTimNV").onclick = function () {
  var searchValue = document.getElementById("searchName").value;
  var dsLoc = [];
  for (var index = 0; index < dsnv.length; index++) {
    var nhanVienHienTai = dsnv[index];
    if (layXepLoai(nhanVienHienTai).includes(searchValue)) {
      dsLoc.push(nhanVienHienTai);
    }
  }
  renderDsnv(dsLoc, "tableDanhSachDuocLoc");
});
